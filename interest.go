package interests

import (
	"fmt"
	"strings"

	"bitbucket.org/matchmove/utils"
)

// Interest represents interest object
type Interest struct {
	ID               string   `json:"id,omitempty"`
	Type             string   `json:"type,omitempty"`
	Source           string   `json:"source,omitempty"`
	Reference        string   `json:"reference,omitempty"`
	Gender           []string `json:"-"`
	Name             string   `json:"name,omitempty"`
	OriginalScript   string   `json:"original_script,omitempty"`
	Title            string   `json:"title,omitempty"`
	Designation      string   `json:"designation,omitempty"`
	DOB              []string `json:"dob,omitempty"`
	POBTown          string   `json:"pob_town,omitempty"`
	POBCountry       string   `json:"pob_country,omitempty"`
	AliasGoodQuality []string `json:"alias_good_quality,omitempty"`
	AliasLowQuality  []string `json:"alias_low_quality,omitempty"`
	Nationality      []string `json:"nationality,omitempty"`
	Address          []string `json:"address,omitempty"`
	ListedOn         string   `json:"listed_on,omitempty"`
	LastUpdated      string   `json:"last_updated,omitempty"`
	OtherInformation string   `json:"other_information,omitempty"`
	Country          []string `json:"country,omitempty"`
	Documents        []string `json:"documents,omitempty"`
	Regime           string   `json:"regime,omitempty"`
}

// Interests an array of Interest
type Interests []Interest

const (
	// InterestTypeIndividual interest type Individuals
	InterestTypeIndividual = "Individual"
	// InterestTypeEntities interest type Entities
	InterestTypeEntities = "Entity"
	// CloudSearchToken format used to generate cloud search ids
	CloudSearchToken = "%s-%s-%s-%s-%s"
)

// GenerateID ...
func (i *Interest) GenerateID() string {
	token := fmt.Sprintf(CloudSearchToken, i.Source, i.ID, i.Name, strings.Join(i.AliasGoodQuality, "-"), i.Type)
	hash := utils.Hash{S: token}
	return hash.MD5()
}
